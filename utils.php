<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * @param GuzzleHttp\Client $request
 * @param type $base
 * @param type $symbols
 * @throws Exception
 */
function GetLatestExchangeRates($client, $base, $symbols = array()) {
    
    static $promises = array();
    
    if(is_array($symbols)) {
        $symbols = implode(',', $symbols);
    } else {
        throw new Exception("Symbols is not valid, it must either be empty or an array");
    }
    
    $query = http_build_query(array(
        'base' => $base,
        'symbols' => $symbols
    ));
    
    array_push($promises, $client->getAsync('?'.$query));

    return $promises;
}