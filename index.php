<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require 'vendor/autoload.php';

require_once 'utils.php';

use GuzzleHttp\Client;
use GuzzleHttp\Promise;

define('CLIENT_EXCHANGE', 'api.exchangeratesapi.io');

$client = new Client(['base_uri' => 'https://'.CLIENT_EXCHANGE.'/latest']);

GetLatestExchangeRates($client, 'USD', array('GBP', 'EUR'));
GetLatestExchangeRates($client, 'INR', array('GBP', 'EUR'));
GetLatestExchangeRates($client, 'EUR', array('GBP', 'USD'));
$promises = GetLatestExchangeRates($client, 'GBP', array('INR', 'EUR'));

$results = Promise\unwrap($promises);
$results = Promise\settle($promises)->wait();

foreach($results as $result) {
    echo $result['value']->getBody() . "\n";
}